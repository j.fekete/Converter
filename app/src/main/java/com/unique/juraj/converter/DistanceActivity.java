package com.unique.juraj.converter;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DistanceActivity extends Activity {
    @BindView(R.id.etDistance)
    EditText Distance;
    @BindView(R.id.bCalculate)
    Button Calculate;
    @BindView(R.id.tvResult)
    TextView Result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.bCalculate)
    public void OnClick(View view) {
        double distance;
        double result;
        String text="Input is empty";
        String msg = "";

        try{
            distance= Double.parseDouble(Distance.getText().toString());
            result=distance*0.621371192;
            Result.setText(String.valueOf(result));

        }catch (Exception e ){
            this.showToast(text);
        }

    }




    private void showToast(String text){
        Toast toast = Toast.makeText(this,text,Toast.LENGTH_LONG);
        toast.show();
    }
}
