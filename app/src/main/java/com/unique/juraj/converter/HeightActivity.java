package com.unique.juraj.converter;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HeightActivity extends Activity {
    @BindView(R.id.etHeight)
    EditText Height;
    @BindView(R.id.bCalculate)
    Button Calculate;
    @BindView(R.id.tvResult)
    TextView Result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_height);
        ButterKnife.bind(this);

    }
    @OnClick(R.id.bCalculate)
    public void OnClick(View view) {
        double height;
        double result;
        String text="Input is empty";
        String msg = "";

        try{
            height= Double.parseDouble(Height.getText().toString());
            result=height*0.032808399;
            Result.setText(String.valueOf(result));

        }catch (Exception e ){
            this.showToast(text);
        }

    }




    private void showToast(String text){
        Toast toast = Toast.makeText(this,text,Toast.LENGTH_LONG);
        toast.show();
    }

}
