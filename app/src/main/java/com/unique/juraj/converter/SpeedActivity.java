package com.unique.juraj.converter;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SpeedActivity extends Activity {
    @BindView(R.id.etSpeed)
    EditText Speed;
    @BindView(R.id.bCalculate)
    Button Calculate;
    @BindView(R.id.tvResult)
    TextView Result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.bCalculate)
    public void OnClick(View view) {
        double speed;
        double result;
        String text="Input is empty";
        String msg = "";

        try{
            speed = Double.parseDouble(Speed.getText().toString());
            result=speed*0.621371192;
            Result.setText(String.valueOf(result));

        }catch (Exception e ){
            this.showToast(text);
        }

    }




    private void showToast(String text){
        Toast toast = Toast.makeText(this,text,Toast.LENGTH_LONG);
        toast.show();
    }
}


