package com.unique.juraj.converter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @BindView(R.id.ivDistance)
    ImageButton Distance;
    @BindView(R.id.ivHeight) ImageButton Height;
    @BindView(R.id.ivSpeed)  ImageButton Speed;
    @BindView(R.id.ivWeight) ImageButton Weight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.ivDistance, R.id.ivHeight, R.id.ivSpeed, R.id.ivWeight})
    public void OnClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()){
            case R.id.ivDistance:
                intent.setClass(this,DistanceActivity.class);
                startActivity(intent);
                break;
            case R.id.ivHeight:
                intent.setClass(this,HeightActivity.class);
                startActivity(intent);
                break;
            case  R.id.ivSpeed:
                intent.setClass(this,SpeedActivity.class);
                startActivity(intent);
                break;
            case R.id.ivWeight:
                intent.setClass(this,WeightActivity.class);
                startActivity(intent);
                break;
        }
    }


}



