package com.unique.juraj.converter;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WeightActivity extends Activity {
@BindView(R.id.etWeight)
    EditText Weight;
    @BindView(R.id.bCalculate)
    Button Calculate;
    @BindView(R.id.tvResult)
    TextView Result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.bCalculate)
    public void OnClick(View view) {
        double weight;
        double result;
        String text="Input is empty";
        String msg = "";

        try{
            weight = Double.parseDouble(Weight.getText().toString());
            result=weight*2.20462262;
            Result.setText(String.valueOf(result));

        }catch (Exception e ){
            this.showToast(text);
        }

    }




    private void showToast(String text){
        Toast toast = Toast.makeText(this,text,Toast.LENGTH_LONG);
        toast.show();
    }
}
